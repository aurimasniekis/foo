require "foo/version"

module Foo

  class << self

    def run

      name = ARGV[0]

      unless name
        puts "not specified module"
        exit
      end

      begin
        require "foo-#{name}"
      rescue LoadError => e
        raise "Could not find #{name} (#{e})"
      end

      obj = Foo.const_get(name.to_s.capitalize)
      obj.run
    end

  end

end
